// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore'

// import { initializeApp } from "firebase/app";
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

// // Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCWuaS8Lhia_N0H-1cW5ZGQNYIPQSnm9zc",
  authDomain: "restaurant-g33.firebaseapp.com",
  projectId: "restaurant-g33",
  storageBucket: "restaurant-g33.appspot.com",
  messagingSenderId: "732240506443",
  appId: "1:732240506443:web:db3b254ad10017fd4ae7ce"
};

firebase.initializeApp(firebaseConfig)

const db = firebase.firestore(); // axios.get axios.post db.get db.post

export {db, firebase}