import Rutas from "./routes";
// import AuthState from "./context/auth/authState";
// import ProductState from "./context/products/productState";

import { Provider } from "react-redux";
// import { store } from "./reduxApp/store";
import {store} from './features/store'

function App() {
  return (
    <Provider store={store}>
      {/* <AuthState> */}
      {/* <ProductState> */}
        <Rutas />
      {/* </ProductState> */}
      {/* </AuthState> */}
    </Provider>
  );
}

export default App;
