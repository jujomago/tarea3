import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const RowTable = ({ info, indice, elimiar }) => {
  const { id, nombre, precio, categoria } = info;
  const {deletingItem} = useSelector(state=>state.product);
  return (
    <tr>
      <th scope="row">{indice + 1}</th>
      <td>{nombre}</td>
      <td>{categoria}</td>
      <td>S/ {precio}</td>
      <td className="d-flex justify-content-around">
        <Link className="btn btn-warning" to={`/editar-plato/${id}`}>
          Editar
        </Link>
        <Link className="btn btn-primary" to={`/ver-detalle/${id}`}>Ver detalle</Link>
        <button className="btn btn-danger" onClick={elimiar} disabled={(deletingItem===id)}>
          Eliminar
          {(deletingItem===id) && (
             " " && <span className="spinner-border spinner-border-sm"></span>
          )}
        </button>
      </td>
    </tr>
  );
};

export default RowTable;
