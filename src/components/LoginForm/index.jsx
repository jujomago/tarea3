import { useContext, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { iniciarSesion } from "../../reduxApp/actions/auth";
import { iniciarSesion } from "../../features/authSlice";
// import AuthContext from "../../context/auth/authContext";

const LoginForm = () => {
  const [formValues, setFormValues] = useState({
    email: "",
    password: "",
  });

  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.auth);

  // const {
  //   iniciarSesion,
  //   state: { loading },
  // } = useContext(AuthContext);

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("mandamos a llamar el iniciar sesion");
    const { email, password } = formValues;
    dispatch(iniciarSesion(email, password));
  };

  return (
    <form className="border border-1 p-5" onSubmit={handleSubmit}>
      <h2 className="p-3 text-center">Hola, Bienvenid@s</h2>
      <div className="mb-3">
        <label htmlFor="email" className="form-label">
          Correo
        </label>
        <input
          type="email"
          className="form-control"
          name="email"
          onChange={handleChange}
          value={formValues.email}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="password" className="form-label">
          Contraseña
        </label>
        <input
          type="password"
          className="form-control"
          name="password"
          onChange={handleChange}
          value={formValues.password}
        />
      </div>

      <div className="d-grid gap-2 mx-auto">
        <button type="submit" className="btn btn-primary">
          Iniciar Sesión{" "}
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
        </button>
      </div>
    </form>
  );
};

export default LoginForm;
