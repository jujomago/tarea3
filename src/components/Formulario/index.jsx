import { useEffect, useState } from "react";
import Error from "./Error";
const categorias = ["Bebidas", "Cortes", "Entradas", "Postres", "Sopas", "Pastas", "China"];

const initialState = {
  nombre: "",
  precio: "",
  categoria: "",
  descripcion: "",
};

const errorInit = {
  ...initialState,
};

const Formuario = ({
  agregarProducto,
  loading,
  editar = false,
  producto,
  isDetail = false,
  editarProducto,
}) => {
  const [dataForm, setDataForm] = useState(initialState);
  const [errors, setErrors] = useState(errorInit);

  useEffect(() => {
    if (editar) {
      setDataForm(producto);
    }
  }, []);

  const handleChange = (e) => {
    setDataForm({ ...dataForm, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: "" });
  };

  const isValid = () => {
    let respuesta = true;
    const localErrors = { ...errorInit };
    for (let key in dataForm) {
      if (key !== "id") {
        if (dataForm[key].trim() === "" || dataForm[key].length === 0) {
          localErrors[key] = "Campo requerido";
          respuesta = false;
        }
      }
    }
    setErrors(localErrors);

    return respuesta;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isValid()) {
      if (editar) {
        console.log("llama a la funcion de editar");
        editarProducto(dataForm);
      } else {
        agregarProducto(dataForm);
      }
    }
  };

  return (
    <form className="row g-3" onSubmit={handleSubmit}>
      <div className="col-md-6">
        <label htmlFor="dish" className="form-label">
          Nombre del plato
        </label>
        {isDetail && <h4>{dataForm.nombre}</h4>}
        {!isDetail && <input
          type="text"
          className="form-control"
          name="nombre"
          onChange={handleChange}
          value={dataForm.nombre}
        />}
        <Error text={errors.nombre} />
      </div>
      <div className="col-md-6">
        <label htmlFor="price" className="form-label">
          precio
        </label>
        {isDetail && <h5>{dataForm.precio}</h5>}
        {!isDetail &&
        <input
          type="number"
          className="form-control"
          name="precio"
          onChange={handleChange}
          value={dataForm.precio}
        />}
        <Error text={errors.precio} />
      </div>
      <div className="col-6">
        <label htmlFor="inputAddress" className="form-label">
          Categoría
        </label>
        {isDetail && <h5>{dataForm.categoria}</h5>}
        {!isDetail &&
        <select
          className="form-select"
          name="categoria"
          onChange={handleChange}
          value={dataForm.categoria}
        >
          <option disabled value="">
            Selecciona una Categoría
          </option>
          {categorias.map((categoria) => (
            <option key={categoria} value={categoria}>
              {categoria}
            </option>
          ))}
        </select>}
        <Error text={errors.categoria} />
      </div>
      <div className="col-md-6">
        <label htmlFor="exampleFormControlTextarea1" className="form-label">
          Descripción
        </label>
        {isDetail && <h5>{dataForm.descripcion}</h5>}
        {!isDetail && <textarea
          className="form-control"
          id="exampleFormControlTextarea1"
          rows="3"
          name="descripcion"
          onChange={handleChange}
          value={dataForm.descripcion}
        ></textarea> }
        <Error text={errors.descripcion} />
      </div>
      <div className="col-6">
       {!isDetail &&  <button type="submit" className="btn btn-primary">
          {editar ? "Editar Plato" : "Crear Plato"}
          {loading && (
            <span className="spinner-border spinner-border-sm"></span>
          )}
        </button>}
      </div>
    </form>
  );
};

export default Formuario;
