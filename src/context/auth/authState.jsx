import { useReducer, useEffect } from "react";
import AuthContext from "./authContext";
import reducer from "./authReducer";

import { firebase } from "../../firebase";

const AuthState = ({ children }) => {
  const initialState = {
    loading: false,
    isAuth: false,
    email: "",
    consultando: true,
  };

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      console.log("miramos el user", user);
      let autenticado;
      if (user?.uid) {
        autenticado = true;
      } else {
        autenticado = false;
      }
      dispatch({
        type: "UPDATE_IS_AUTH",
        payload: {
          isAuth: autenticado,
          email: user ? user.email : "",
          consultando: false,
        },
      });
    });
  }, []);

  const iniciarSesion = async (email, password) => {
    dispatch({ type: "LOADER", payload: true });
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      dispatch({
        type: "LOGIN",
        payload: { email: user.email, isAuth: true },
      });
    } catch (error) {
      console.log("mira el error", error);
    } finally {
      dispatch({ type: "LOADER", payload: false });
    }
  };

  const cerrarSesion = async () => {
    try {
      await firebase.auth().signOut();
    } catch (error) {
      console.log("error aqui ", error);
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthContext.Provider value={{ state, iniciarSesion, cerrarSesion }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthState;
