import React, { useReducer } from "react";
import ProductContext from "./productContext";
import reducer from "./productReducer";

import { db } from "../../firebase";

const ProductState = ({ children }) => {
  const initialState = {
    loading: false,
    addOk: false,
    listaProductos: [],
    producto: {},
    editOk: false,
    deleteOk: false,
  };

  /**
   *
   * state.listaProductos.find((item) => item.name === producto.name);
   */

  const agregarProducto = async (producto) => {
    dispatch({
      type: "LOADER",
      payload: true,
    });
    // verificarProducto(producto);
    try {
      await db.collection("productos").add(producto);
      dispatch({
        type: "AGREGAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log("error al agregar un producto", error);
    } finally {
      dispatch({
        type: "LOADER",
        payload: false,
      });
    }
  };

  const listarProductos = async () => {
    try {
      const productos = [];
      const info = await db.collection("productos").get();
      // console.log("mira los productos", info);
      info.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });
      dispatch({
        type: "LLENAR_PRODUCTOS",
        payload: productos,
      });
    } catch (error) {
      console.log("hubo un error al listar los productos", error);
    }
  };

  const obtenerProducto = async (id) => {
    try {
      const info = await db.collection("productos").doc(id).get();
      const producto = {
        id: info.id,
        ...info.data(),
      };
      dispatch({
        type: "OBTENER_PRODUCTO",
        payload: producto,
      });
    } catch (error) {
      console.log("error al obtener un producto", error);
    }
  };

  const editarProducto = async (producto) => {
    console.log("el producto a editar es: ", producto);
    dispatch({
      type: "LOADER",
      payload: true,
    });
    try {
      const productUpdate = { ...producto };
      delete productUpdate.id;

      await db.collection("productos").doc(producto.id).update(productUpdate);
      dispatch({
        type: "EDITAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log("el error es", error);
    } finally {
      dispatch({
        type: "LOADER",
        payload: false,
      });
    }
  };

  const eliminarProducto = async (id) => {
    try {
      await db.collection("productos").doc(id).delete();
      dispatch({
        type: "ELIMINAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ProductContext.Provider
      value={{
        loading: state.loading,
        addOk: state.addOk,
        editOk: state.editOk,
        deleteOk: state.deleteOk,
        listaProductos: state.listaProductos,
        producto: state.producto,
        agregarProducto,
        listarProductos,
        obtenerProducto,
        editarProducto,
        eliminarProducto,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

export default ProductState;
