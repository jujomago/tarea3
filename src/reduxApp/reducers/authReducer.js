const initialState = {
  loading: false,
  isAuth: false,
  email: "",
  consultando: true,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOADER":
      return {
        ...state,
        loading: action.payload,
      };
    case "LOGIN":
      return {
        ...state,
        email: action.payload.email,
        isAuth: action.payload.isAuth,
      };
    case "UPDATE_IS_AUTH":
      return {
        ...state,
        email: action.payload.email,
        isAuth: action.payload.isAuth,
        consultando: action.payload.consultando,
      };

    default:
      return state;
  }
};
