import { db } from "../../firebase";
export const agregarProducto = (producto) => {
  return async (dispatch) => {
    dispatch({
      type: "LOADER",
      payload: true,
    });
    // verificarProducto(producto);
    try {
      await db.collection("productos").add(producto);
      dispatch({
        type: "AGREGAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log("error al agregar un producto", error);
    } finally {
      dispatch({
        type: "LOADER",
        payload: false,
      });
    }
  };
};

export const listarProductos = () => {
  return async (dispatch) => {
    try {
      const productos = [];
      const info = await db.collection("productos").get();
      // console.log("mira los productos", info);
      info.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });
      dispatch({
        type: "LLENAR_PRODUCTOS",
        payload: productos,
      });
    } catch (error) {
      console.log("hubo un error al listar los productos", error);
    }
  };
};

export const obtenerProducto = (id) => {
  return async (dispatch) => {
    try {
      const info = await db.collection("productos").doc(id).get();
      const producto = {
        id: info.id,
        ...info.data(),
      };
      dispatch({
        type: "OBTENER_PRODUCTO",
        payload: producto,
      });
    } catch (error) {
      console.log("error al obtener un producto", error);
    }
  };
};

export const editarProducto = (producto) => {
  return async (dispatch) => {
    console.log("el producto a editar es: ", producto);
    dispatch({
      type: "LOADER",
      payload: true,
    });
    try {
      const productUpdate = { ...producto };
      delete productUpdate.id;

      await db.collection("productos").doc(producto.id).update(productUpdate);
      dispatch({
        type: "EDITAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log("el error es", error);
    } finally {
      dispatch({
        type: "LOADER",
        payload: false,
      });
    }
  };
};

export const eliminarProducto = (id) => {
  return async (dispatch) => {
    try {
      await db.collection("productos").doc(id).delete();
      dispatch({
        type: "ELIMINAR_PRODUCTO",
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };
};
