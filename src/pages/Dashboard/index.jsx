import { useContext, useEffect } from "react";
// import ProductContext from "../../context/products/productContext";
import { useSelector, useDispatch } from "react-redux";
import { listarProductos, eliminarProducto } from "../../features/productSlice";
// import {
//   listarProductos,
//   eliminarProducto,
// } from "../../reduxApp/actions/product";
import Table from "../../components/Table";

const Dashboard = () => {
  const { listaProductos, deleteOk, loadingProducts } = useSelector((state) => state.product);
  const dispatch = useDispatch();

  // const { listarProductos, listaProductos, eliminarProducto, deleteOk } =
  //   useContext(ProductContext);

  useEffect(() => {
    dispatch(listarProductos());
  }, []);

  useEffect(() => {
    if (deleteOk) {
      dispatch(listarProductos());
    }
  }, [deleteOk]);

  return (
    <>
      {loadingProducts && <div className="text-center">
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
        Loading...
      </div>}
      {!loadingProducts && (<>
        <h1>Productos</h1>
        <Table
          listaProductos={listaProductos}
          eliminarProducto={(id) => dispatch(eliminarProducto(id))}
        /></>)}
    </>
  );
};

export default Dashboard;
