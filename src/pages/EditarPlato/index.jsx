import { useEffect, useContext } from "react";
import { useParams, useNavigate, matchPath, useLocation, useMatch } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { obtenerProducto, editarProducto } from "../../features/productSlice";
// import {
//   obtenerProducto,
//   editarProducto,
// } from "../../reduxApp/actions/product";
// import ProductContext from "../../context/products/productContext";
import Formulario from "../../components/Formulario";

const EditarPlato = () => {
  const params = useParams();
  const {pathname} =  useLocation();
  const match = matchPath({
    path:'/ver-detalle/:editId',
    exact:true,
    strict:true,
  },pathname); 
 

  const { producto, loading, editOk, isDetail } = useSelector((state) => state.product);
  const dispatch = useDispatch();

  const navidate = useNavigate();

  useEffect(() => {
    dispatch(obtenerProducto(params.editId));
  }, []);

  useEffect(() => {
    if (editOk) {
      navidate("/dashboard");
    }
  }, [editOk]);

  console.log("el producto es: ", producto);

  const tieneProducto = Object.keys(producto).length;

  return (
    <div className="row">
      <div className="col-12">
        <h5 className="text-primary text-center py-3">{!match? "Editar Plato":"Detalle del plato"}</h5>
        <div className="shadow-lg p-3 mb-5 bg-body rounded">
          {tieneProducto > 0 ? (
            <Formulario
              producto={producto}
              editar={true}
              isDetail={match??false}
              editarProducto={(producto) => dispatch(editarProducto(producto))}
              loading={loading}
            />
          ) : (
            "Cargando ..."
          )}
        </div>
      </div>
    </div>
  );
};

export default EditarPlato;
