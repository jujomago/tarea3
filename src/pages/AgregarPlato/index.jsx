import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { agregarProducto } from "../../features/productSlice";
// import { agregarProducto } from "../../reduxApp/actions/product";
// import ProductContext from "../../context/products/productContext";
import Formulario from "../../components/Formulario";

const AgregarPlato = () => {
  // const { agregarProducto, loading, addOk } = useContext(ProductContext);
  const { loading, addOk } = useSelector((state) => state.product);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  useEffect(() => {
    if (addOk) {
      navigate("/dashboard");
    }
  }, [addOk]);

  return (
    <div className="row">
      <div className="col-12">
        <h5 className="text-primary text-center py-3">Agregar Nuevo Plato</h5>
        <div className="shadow-lg p-3 mb-5 bg-body rounded">
          <Formulario
            agregarProducto={(producto) => dispatch(agregarProducto(producto))}
            loading={loading}
          />
        </div>
      </div>
    </div>
  );
};

export default AgregarPlato;
