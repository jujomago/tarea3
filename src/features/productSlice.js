import { createSlice } from "@reduxjs/toolkit";
import { db } from "../firebase";

const initialState = {
  loading: false,
  addOk: false,
  listaProductos: [],
  loadingProducts: false,
  deletingItem: null,
  producto: {},
  editOk: false,
  deleteOk: false,
};

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    loader(state, action) {
      state.loading = action.payload;
    },
    agregarUnProducto(state, action) {
      state.addOk = action.payload;
    },
    llenarProductos(state, action) {
      state.addOk = false;
      state.editOk = false;
      state.deleteOk = false;
      state.listaProductos = action.payload;
      state.producto = {};
    },
    obtenerUnProducto(state, action) {
      state.producto = action.payload;
    },
    editarUnProducto(state, action) {
      state.editOk = action.payload;
    },
    eliminarUnProducto(state, action) {
      state.deleteOk = action.payload;
    },
    updateLodingProducts(state, action) {
      state.loadingProducts = action.payload;
    },
    updateDeletingItem(state, action) {
      state.deletingItem = action.payload;      
    }
  },
});

export const agregarProducto = (producto) => {
  return async (dispatch) => {
    dispatch(loader(true));
    // verificarProducto(producto);
    try {
      await db.collection("productos").add(producto);
      dispatch(agregarUnProducto(true));
    } catch (error) {
      console.log("error al agregar un producto", error);
    } finally {
      dispatch(loader(false));
    }
  };
};



export const listarProductos = () => async (dispatch) => {

  dispatch(updateLodingProducts(true));
  try {
    const productos = [];
    const info = await db.collection("productos").get();
    // console.log("mira los productos", info);
    info.forEach((item) => {
      productos.push({
        id: item.id,
        ...item.data(),
      });
    });
    dispatch(llenarProductos(productos));
  } catch (error) {
    console.log("hubo un error al listar los productos", error);
  } finally {
    dispatch(updateLodingProducts(false));
  }

};

export const obtenerProducto = (id) => {
  return async (dispatch) => {
    try {
      const info = await db.collection("productos").doc(id).get();
      const producto = {
        id: info.id,
        ...info.data(),
      };
      dispatch(obtenerUnProducto(producto));
    } catch (error) {
      console.log("error al obtener un producto", error);
    }
  };
};

export const editarProducto = (producto) => {
  return async (dispatch) => {
    console.log("el producto a editar es: ", producto);
    dispatch(loader(true));
    try {
      const productUpdate = { ...producto };
      delete productUpdate.id;

      await db.collection("productos").doc(producto.id).update(productUpdate);
      dispatch(editarUnProducto(true));
    } catch (error) {
      console.log("el error es", error);
    } finally {
      dispatch(loader(false));
    }
  };
};

export const eliminarProducto = (id) => async (dispatch) => {
 dispatch(updateDeletingItem(id));
  try {
    await db.collection("productos").doc(id).delete();
    dispatch(eliminarUnProducto(true));
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(updateDeletingItem(null))
  }

};

export const {
  loader,
  agregarUnProducto,
  llenarProductos,
  obtenerUnProducto,
  editarUnProducto,
  eliminarUnProducto,
  updateLodingProducts,
  updateDeletingItem
} = productSlice.actions;
export default productSlice.reducer;
