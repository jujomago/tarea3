import { createSlice } from "@reduxjs/toolkit";
import { firebase } from "../firebase";

const initialState = {
  loading: false,
  isAuth: false,
  email: "",
  consultando: true,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loader(state, action) {
      state.loading = action.payload;
    },
    login(state, action) {
      state.email = action.payload.email;
      state.isAuth = action.payload.isAuth;
    },
    updateIsAuth(state, action) {
      state.email = action.payload.email;
      state.isAuth = action.payload.isAuth;
      state.consultando = action.payload.consultando;
    },
    logout(state) {
      state.isAuth = false;
      state.email = null;
    },
  },
});

export const validateAuth = () => {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged((user) => {
      console.log("miramos el user", user);
      let autenticado;
      if (user?.uid) {
        autenticado = true;
      } else {
        autenticado = false;
      }
      dispatch(
        updateIsAuth({
          isAuth: autenticado,
          email: user ? user.email : "",
          consultando: false,
        })
      );
      //   dispatch({
      //     type: "UPDATE_IS_AUTH",
      //     payload: {
      //       isAuth: autenticado,
      //       email: user ? user.email : "",
      //       consultando: false,
      //     },
      //   });
    });
  };
};

export const iniciarSesion = (email, password) => {
  return async (dispatch) => {
    dispatch(loader(true));
    // dispatch({ type: "LOADER", payload: true });
    try {
      const { user } = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      dispatch(login({ email: user.email, isAuth: true }));

      //   dispatch({
      //     type: "LOGIN",
      //     payload: { email: user.email, isAuth: true },
      //   });
    } catch (error) {
      console.log("mira el error", error);
    } finally {
      dispatch(loader(false));
      //   dispatch({ type: "LOADER", payload: false });
    }
  };
};

export const cerrarSesion = () => {
  return async (dispatch) => {
    try {
      await firebase.auth().signOut();
      dispatch(logout());
    } catch (error) {
      console.log("error aqui ", error);
    }
  };
};

export const { updateIsAuth, loader, login, logout } = authSlice.actions;
export default authSlice.reducer;
