import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { validateAuth } from "../features/authSlice";
// import { validateAuth } from "../reduxApp/actions/auth";

import { ListRoute } from "./data";
import { useEffect } from "react";
const Router = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(validateAuth());
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        {ListRoute.map((route, index) => (
          <Route key={index} path={route.path} element={route.element} />
        ))}
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
