import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { cerrarSesion } from "../reduxApp/actions/auth";

// import AuthContext from "../context/auth/authContext";

import Header from "../components/Header";
import Siderbard from "../components/Siderbard";

const PrivateRoute = ({ children }) => {
  const { isAuth, consultando, email } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  // const {
  //   state: { isAuth, consultando, email },
  //   cerrarSesion,
  // } = useContext(AuthContext);

  if (consultando) return <h1>Cargando ....</h1>;

  return (
    <section className="d-flex">
      <Siderbard />
      <div style={{ width: "100%" }}>
        <Header email={email} cerrar={dispatch(cerrarSesion)} />
        <main className="container">
          {isAuth ? children : <Navigate to="/" />}
        </main>
      </div>
    </section>
  );
};

export default PrivateRoute;
