import PublicRoute from "./PublicRoute";
import PrivateRoute from "./PrivateRoute";

import Login from "../pages/Login";
import Dashboard from "../pages/Dashboard";
import AgregarPlato from "../pages/AgregarPlato";
import EditarPlato from "../pages/EditarPlato";

export const ListRoute = [
  {
    path: "/",
    element: (
      <PublicRoute>
        <Login />
      </PublicRoute>
    ),
  },
  {
    path: "/dashboard",
    element: (
      <PrivateRoute>
        <Dashboard />
      </PrivateRoute>
    ),
  },
  {
    path: "/agregar-plato",
    element: (
      <PrivateRoute>
        <AgregarPlato />
      </PrivateRoute>
    ),
  },
  {
    path: "/editar-plato/:editId",
    element: (
      <PrivateRoute>
        <EditarPlato />
      </PrivateRoute>
    ),
  },
  {
    path: "/ver-detalle/:editId",
    element: (
      <PrivateRoute>
        <EditarPlato />
      </PrivateRoute>
    ),
  },
];
